
/*Household TABLE CREATION: */
/*
CREATE TABLE Household
(
  ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL,
  Name VARCHAR(50) NOT NULL,
  Passwd VARCHAR(50) NOT NULL
);
*/

/*Budget_Item TABLE CREATION: */
/*
CREATE TABLE Budget_Item
(
  ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL,
  Name VARCHAR(50) NOT NULL,
  Details VARCHAR(250),
  IOType BIT NOT NULL,
  Value FLOAT NOT NULL,
  TDate DATE NOT NULL,
  Author VARCHAR(50) NOT NULL,
  Category VARCHAR(30) NOT NULL,
  Household_ID INT NOT NULL,
  FOREIGN KEY (Household_ID) REFERENCES Household(ID)
);
*/

/*Household ITEMS SELECTION: */
/*
SELECT * FROM Household
*/

/*Budget_Item ITEMS SELECTION: */
/*
SELECT * FROM Budget_Item
*/

/*Household TABLE INSERTION: */
/*
INSERT INTO Household (Name, Passwd)
VALUES ('Stonks', 'stonk123')
*/

/*Budget_Item TABLE INSERTION: */
/*
INSERT INTO Budget_Item (Name, Details, IOType, Value, TDate, Author, Category, Household_ID)
VALUES ('vyplata', 'mesicni prijem', 1, 24900.00, '2020-03-05', 'Pepa', 'jine', 1)
*/

/*Budget_Item JOINED SELECTION: */
/*
SELECT Budget_Item.Name FROM Budget_Item 
JOIN Household ON Household.ID = Budget_Item.Household_ID
*/

/*Budget_Item UPDATE: */
/*
UPDATE Budget_Item
SET Name = 'rohliky'
WHERE Name = 'Rohliky'
*/
