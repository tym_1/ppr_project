﻿using PPR_Project.Models;
using PPR_Project.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace PPR_Project
{
    /// <summary>
    /// Interakční logika pro App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            
            log.Info("---------------------App start--------------------------");

            MainWinView mainWin = new MainWinView();
            mainWin.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            Manager.Instance.CloseConnection();
        }
    }
}
