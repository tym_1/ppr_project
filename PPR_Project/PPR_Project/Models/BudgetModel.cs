﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPR_Project.Models
{
    public class BudgetModel
    {
        public BudgetModel()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Details { get; set; }
        public bool Type { get; set; }
        public decimal Value { get; set; }
        public DateTime Date { get; set; }
        public string Category { get; set; }
        
    }
}
