﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPR_Project.Models
{
    public class HouseholdModel
    {
        public HouseholdModel(int id, string houseHoldName, string passwordHash)
        {
            Id = id;
            Name = houseHoldName;
            Password = passwordHash;
        }

        public string Name { get; set; }
        public int Id { get; set; }
        public string Password { get; set; }
    }
}
