﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPR_Project.Models
{
    public class Manager
    {
        public Manager()
        {
            Settings = new SettingsModel();
        }

        #region Singleton

        private static volatile Manager _instance;
        public static Manager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Manager();

                return _instance;
            }
        }

        #endregion

        #region Properties

        public SqlConnection SqlConnection { get; private set; }
        public HouseholdModel LoggedInHouseHold { get; set; }
        public SettingsModel Settings { get; set; }

        public bool LoggedIn { get; set; }


        #endregion

        #region PublicMethods

        public bool CreateConnection()
        {
            try
            {
                if (SqlConnection == null)
                    SqlConnection = new SqlConnection("Server=databaze.fai.utb.cz;Database=A17133_A6PPR;" +
                        "User ID=A17133;Password=A17133;persist security info=True");
                if(SqlConnection.State == System.Data.ConnectionState.Closed)
                    SqlConnection.Open();
                if (SqlConnection.State == System.Data.ConnectionState.Open)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public void CloseConnection()
        {
            if(SqlConnection.State == System.Data.ConnectionState.Open)
                SqlConnection.Close();
        }

        #endregion

    }
}
