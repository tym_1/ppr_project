﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPR_Project.Models
{
    public class OverViewModel
    {
        public OverViewModel()
        {
            Incomes = new ObservableCollection<BudgetModel>();
            Outcomes = new ObservableCollection<BudgetModel>();
        }
        public OverViewModel(string name, ObservableCollection<BudgetModel> incomes, ObservableCollection<BudgetModel> outcomes,
            DateTime dateFrom, DateTime dateTo)
        {
            Name = name;
            Incomes = incomes;
            Outcomes = outcomes;
            DateFrom = dateFrom;
            DateTo = dateTo;
        }

        public string Name { get; set; }
        public ObservableCollection<BudgetModel> Incomes { get; set; }
        public ObservableCollection<BudgetModel> Outcomes { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
