﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PPR_Project.Models
{
    public class SettingsModel
    {
        public SettingsModel()
        {
            DeserializeSettings();
        }

        private string XmlPath
        {
            get
            {
                if (!Directory.Exists($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\HomemadeStonks"))
                    Directory.CreateDirectory($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\HomemadeStonks");
                return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\HomemadeStonks\\Settings.xml";
            }
        }

        public bool RememberNameIsChecked { get; set; }
        public string HouseholdName { get; set; }

        public void SerializeSettings()
        {
            XmlDocument xml = new XmlDocument();
            xml.AppendChild(xml.CreateXmlDeclaration("1.0", "utf-8", null));

            XmlNode rootNode = xml.CreateElement("AppSettings");

            XmlNode node = xml.CreateElement("RememberName");
            node.InnerText = RememberNameIsChecked.ToString();
            rootNode.AppendChild(node);

            node = xml.CreateElement("HouseholdName");
            node.InnerText = HouseholdName;
            rootNode.AppendChild(node);

            xml.AppendChild(rootNode);
            xml.Save(XmlPath);
        }

        private void DeserializeSettings()
        {
            try
            {
                if (!File.Exists(XmlPath))
                    return;

                XmlDocument xml = new XmlDocument();
                xml.Load(XmlPath);

                XmlNode rootNode = xml.SelectSingleNode("/AppSettings");
                foreach (XmlNode node in rootNode.ChildNodes)
                {
                    switch(node.Name)
                    {
                        case "RememberName":
                            RememberNameIsChecked = node.InnerText == "True" ? true : false; break;
                        case "HouseholdName":
                            HouseholdName = node.InnerText; break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

    }
}
