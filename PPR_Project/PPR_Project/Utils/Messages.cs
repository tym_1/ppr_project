﻿using System.Windows;

namespace PPR_Project.Utils
{
    class Messages
    {
        public static void MsgWarning(string text)
        {
            if (Application.Current.MainWindow == null)
                MessageBox.Show(text, "Varování", MessageBoxButton.OK, MessageBoxImage.Warning);
            else
                MessageBox.Show(Application.Current.MainWindow, text, "Varování", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public static void MsgError(string text)
        {
            if (Application.Current.MainWindow == null)
                MessageBox.Show(text, "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
            else
                MessageBox.Show(Application.Current.MainWindow, text, "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static bool MsgYesNo(string text)
        {
            if (Application.Current.MainWindow == null)
                return MessageBox.Show(text, "Dotaz", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
            return MessageBox.Show(Application.Current.MainWindow, text, "Dotaz", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
        }

        public static void MsgInfo(string text)
        {
             MessageBox.Show(text, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static bool MsgOkCancel(string text)
        {
            if (Application.Current.MainWindow == null)
                return MessageBox.Show(text, "Info", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK;
            return MessageBox.Show(Application.Current.MainWindow, text, "Info", MessageBoxButton.OKCancel, MessageBoxImage.Information) == MessageBoxResult.OK;
        }
    }
}
