﻿using PPR_Project.Models;
using PPR_Project.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PPR_Project.ViewModels
{
    public class AddNewBudgetViewModel : BaseViewModel
    {
        // TODO: Zde stačí jen zkontrolovat zda jsou vyplněny všechna povinná pole a udělat příkaz na přidání do databáze
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private MainWinViewModel _vmMainWin;
        public AddNewBudgetViewModel(MainWinViewModel vmMainWin)
        {
            _vmMainWin = vmMainWin;
            BudgetTypes = new ObservableCollection<string>
            {
                "Příjem",
                "Výdaj"
            };
            
            SelectedType = BudgetTypes.First();
            CommandAddBudget = new CommandGeneric(AddBudget);

            Date = DateTime.Now;
        }

        #region Properties

        private ObservableCollection<string> _budgetTypes;
        public ObservableCollection<string> BudgetTypes { get => _budgetTypes; set { _budgetTypes = value; OnPropertyChanged(nameof(BudgetTypes)); } }
        private string _selectedType;
        public string SelectedType { get => _selectedType; set { _selectedType = value; OnPropertyChanged(nameof(SelectedType)); } }
        private string _budgetName;
        public string BudgetName { get => _budgetName; set { _budgetName = value; OnPropertyChanged(nameof(BudgetName)); } }
        private string _authorName;
        public string AuthorName { get => _authorName; set { _authorName = value; OnPropertyChanged(nameof(AuthorName)); } }
        private string _budgetDetails;
        public string BudgetDetails { get => _budgetDetails; set { _budgetDetails = value; OnPropertyChanged(nameof(BudgetDetails)); } }
        private decimal _budgetValue;
        public decimal BudgetValue { get => _budgetValue; set { _budgetValue = value; OnPropertyChanged(nameof(BudgetValue)); } }
        private string _budgetCategory;
        public DateTime Date { get => _date; set { _date = value; OnPropertyChanged(nameof(Date)); } }
        private DateTime _date;
        public string BudgetCategory { get => _budgetCategory; set { _budgetCategory = value; OnPropertyChanged(nameof(BudgetCategory)); } }

        #endregion

        #region Commands

        public ICommand CommandAddBudget { get; set; }

        public async void AddBudget()
        {
            try
            {
                string error = string.Empty;
                if (String.IsNullOrWhiteSpace(BudgetName))
                {
                    error += "Nevyplnili jste název položky!\n";
                }
                if (String.IsNullOrWhiteSpace(AuthorName))
                {
                    error += "Nevyplnili jste autora!\n";
                }
                if (BudgetValue <= 0)
                {
                    error += "Číselná hodnota nesmí být záporná nebo nulová!\n";
                }
                if (String.IsNullOrWhiteSpace(BudgetCategory))
                {
                    error += "Nevyplnili jste kategorii!\n";
                }

                if (error.Length == 0)
                {
                    //OK

                    var selection = $"Insert INTO Budget_Item (Name, Details, IOType, Value, TDate, Author, Category, Household_ID)  " +
                        $"VALUES (\'{ BudgetName }\', \'{ BudgetDetails }\', \'{ (SelectedType == "Příjem" ? 1 : 0) }\', \'{ BudgetValue }\', \'{ Date.ToString("yyyy-MM-dd") }\', \'{ AuthorName }\', \'{ BudgetCategory }\', \'{ Manager.Instance.LoggedInHouseHold.Id }\')";
                    SqlCommand selectionCommand = new SqlCommand(selection, Manager.Instance.SqlConnection);
                    await selectionCommand.ExecuteNonQueryAsync();

                    Messages.MsgInfo("Položka úspěšně přidána.");
                    BudgetName = BudgetDetails = AuthorName = BudgetCategory =  string.Empty;
                    BudgetValue = 0;
                    Date = DateTime.Now;
                }
                else 
                {
                    Messages.MsgError("Nelze přidat položku: \n" + error);
                }
            }
            catch (Exception ex)
            {
                Messages.MsgError("Nepodařilo se přidat novou položku!");
                log.Error(ex, "AddBudget() failed!!!");
            }
        }

        #endregion

        #region PrivateMethods


        #endregion
    }
}
