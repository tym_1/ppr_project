﻿using PPR_Project.Models;
using PPR_Project.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PPR_Project.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private MainWinViewModel _vmMainWin;
        public LoginViewModel(MainWinViewModel vmMainWin)
        {
            _vmMainWin = vmMainWin;
            InitCommands();
            RememberLoginNameIsChecked = Manager.Instance.Settings.RememberNameIsChecked;
            if (RememberLoginNameIsChecked)
                HouseHoldName = Manager.Instance.Settings.HouseholdName;
        }

        #region Properties

        private string _houseHoldName;
        public string HouseHoldName 
        { 
            get => _houseHoldName; 
            set 
            { 
                _houseHoldName = value; 
                OnPropertyChanged(nameof(HouseHoldName));
                if (RememberLoginNameIsChecked)
                {
                    Manager.Instance.Settings.HouseholdName = value;
                    Manager.Instance.Settings.SerializeSettings();
                }
            } 
        }
        private string _password;
        public string Password { get => _password; set { _password = value; OnPropertyChanged(nameof(Password)); } }
        private string _hewHouseholdName;
        public string NewHouseHoldName {  get => _hewHouseholdName; set { _hewHouseholdName = value; OnPropertyChanged(nameof(NewHouseHoldName)); } }
        private string _newHouseHoldPassword;
        public string NewHouseHoldPassword { get=> _newHouseHoldPassword; set { _newHouseHoldPassword = value; OnPropertyChanged(nameof(NewHouseHoldPassword)); } }
        private string _newHouseHoldPasswordAgain;
        public string NewHouseHoldPasswordAgain { get => _newHouseHoldPasswordAgain; set { _newHouseHoldPasswordAgain = value; OnPropertyChanged(nameof(NewHouseHoldPasswordAgain)); } }

        private bool _rememberLoginNameIsChecked;
        public bool RememberLoginNameIsChecked 
        { 
            get => _rememberLoginNameIsChecked; 
            set 
            { 
                _rememberLoginNameIsChecked = value; 
                OnPropertyChanged(nameof(RememberLoginNameIsChecked));
                if (value && !string.IsNullOrWhiteSpace(HouseHoldName))
                    Manager.Instance.Settings.HouseholdName = HouseHoldName;
                Manager.Instance.Settings.RememberNameIsChecked = value;
                Manager.Instance.Settings.SerializeSettings();
            } 
        }


        #endregion

        #region Commands

        public ICommand CommandLogIn { get; set; }
        public ICommand CommandRegister { get; set; }
        public async void Login()
        {
            try
            {
                var cmdText = $"Select ID, Passwd FROM Household WHERE Name=\'{ HouseHoldName }\'";
                SqlCommand loginCommand = new SqlCommand(cmdText, Manager.Instance.SqlConnection);
                var dataReader =  await loginCommand.ExecuteReaderAsync();
                var Psswd = "";
                int Id = 0;

                while (dataReader.Read())
                {
                    Id = Convert.ToInt32(dataReader.GetValue(0));
                    Psswd = dataReader.GetValue(1).ToString();
                }

                dataReader.Close();

                if (Id == 0)
                {
                    Messages.MsgInfo($"Zadaná domácnost neexistuje");
                    return;
                }

                SHA256 mySHA256 = SHA256.Create();
                var PasswordHash = Convert.ToBase64String(mySHA256.ComputeHash(Encoding.UTF8.GetBytes(Password)));

                if (Psswd.Equals(PasswordHash))
                {
                    log.Info("Successfull login");
                    Manager.Instance.LoggedIn = true;
                    Manager.Instance.LoggedInHouseHold = new HouseholdModel(Id, HouseHoldName, PasswordHash);
                    _vmMainWin.GoToOverview();
                }
                else
                {
                    log.Warn("Login was not Successfull");
                    Utils.Messages.MsgError("Špatné heslo");
                }

            }
            catch (Exception ex)
            {
                log.Warn("Exception in login" + ex);
                Messages.MsgError($"Nepodařilo se přihlásit uživatele - {ex.Message}");
            }
        }

        public async void Register()
        {
            try
            {
                SHA256 mySHA256 = SHA256.Create();
                var PasswordHash = Convert.ToBase64String(mySHA256.ComputeHash(Encoding.UTF8.GetBytes(NewHouseHoldPassword)));
                var PasswordHashAgain = Convert.ToBase64String(mySHA256.ComputeHash(Encoding.UTF8.GetBytes(NewHouseHoldPasswordAgain)));
                if (PasswordHash.Equals(PasswordHashAgain))
                {
                    var cmdText = $"Insert into Household (Name, Passwd) values(\'{ NewHouseHoldName }\',\'{ PasswordHash }\')";
                    SqlCommand loginCommand = new SqlCommand(cmdText, Manager.Instance.SqlConnection);
                    await loginCommand.ExecuteNonQueryAsync();

                    log.Info("Successfull registration of new household");
                    Utils.Messages.MsgInfo("Registrace proběhla úspěšně");

                    cmdText = $"Select ID FROM Household WHERE Name=\'{ NewHouseHoldName }\'";
                    loginCommand = new SqlCommand(cmdText, Manager.Instance.SqlConnection);
                    var dataReader = await loginCommand.ExecuteReaderAsync();
                    int Id = 0;

                    while (dataReader.Read())
                    {
                        Id = Convert.ToInt32(dataReader.GetValue(0));
                    }

                    dataReader.Close();

                    Manager.Instance.LoggedIn = true;
                    Manager.Instance.LoggedInHouseHold = new HouseholdModel(Id, NewHouseHoldName, PasswordHash);

                    _vmMainWin.GoToOverview();
                }
                else
                {
                    log.Warn("Psswds don't match");
                    Utils.Messages.MsgError("hesla se neshodují");
                }
            }
            catch (Exception ex)
            {
                log.Warn("Exception in login" + ex);
                Messages.MsgError($"Nepodařilo se registrovat novou domácnost - {ex.Message}");
            }
        }

        #endregion

        #region PrivateMethods

        private void InitCommands()
        {
            CommandLogIn = new CommandGeneric(Login);
            CommandRegister = new CommandGeneric(Register);
        }

        #endregion


    }
}
