﻿using PPR_Project.Models;
using PPR_Project.Utils;
using PPR_Project.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace PPR_Project.ViewModels
{
    
    public class MainWinViewModel : BaseViewModel
    {
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private MainWinView _mainWinView;
        public MainWinViewModel(MainWinView mainWinView)
        {
            _mainWinView = mainWinView;
            CurrentUserControl = new LoginView(this);
            Manager.Instance.CreateConnection();
            SetLogOutButtonVisibility();
            CommandGoUcAddBudget = new CommandGeneric(GoUcAddBudget);
            CommandLogOut = new CommandGeneric(LogOut);
            CommandGoUcOverview = new CommandGeneric(GoUcOverview);
            CommandGoUcSettings = new CommandGeneric(GoUcSettings);
        }

        #region Properties

        private bool _logOutIsVisible;
        public bool LogOutIsVisible { get => _logOutIsVisible; set { _logOutIsVisible = value; OnPropertyChanged(nameof(LogOutIsVisible)); } }

        #endregion

        #region UserControls

        private UserControl _currentUserControl;
        public UserControl CurrentUserControl { get => _currentUserControl; set { _currentUserControl = value; OnPropertyChanged(nameof(CurrentUserControl)); } }

        #endregion

        #region Commands

        public ICommand CommandLogOut { get; set; }
        public ICommand CommandGoUcAddBudget { get; set; }
        public ICommand CommandGoUcOverview { get; set; }
        public ICommand CommandGoUcSettings { get; set; }

        public void LogOut()
        {
            Manager.Instance.LoggedIn = false;
            Manager.Instance.LoggedInHouseHold = null;
            SetLogOutButtonVisibility();
            CurrentUserControl = new LoginView(this);
        }

        public void GoUcAddBudget()
        {
            try
            {
                CurrentUserControl = new AddNewBudgetView(this);
            }
            catch (Exception ex)
            {
                log.Error(ex, "GoUcAddBudget()");
                Messages.MsgError($"Nepodařilo se zobrazit obrazovku pro přidání nového příjmu/výdaje");
            }
        }

        public void GoUcOverview()
        {
            try
            {
                CurrentUserControl = new OverviewView(this);
            }
            catch (Exception ex)
            {
                log.Error(ex, "GoUcOverview()");
                Messages.MsgError($"Nepodařilo se zobrazit obrazovku pro výpis všech transakcí");
            }
        }

        public void GoUcSettings()
        {
            try
            {
                CurrentUserControl = new SettingsView(this);
            }
            catch (Exception ex)
            {
                log.Error(ex, "GoUcSettings()");
                Messages.MsgError($"Nepodařilo se zobrazit obrazovku pro nastavení");
            }
        }

        #endregion

        #region Methods

        public void SetLogOutButtonVisibility() => LogOutIsVisible = Manager.Instance.LoggedIn;

        public void GoToOverview()
        {
            OverviewView overView = new OverviewView(this);
            LogOutIsVisible = true;
            CurrentUserControl = overView;
        }

        #endregion
    }
}
