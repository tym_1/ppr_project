﻿using PPR_Project.Models;
using PPR_Project.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPR_Project.ViewModels
{
    public class OverviewViewModel : BaseViewModel
    {
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private readonly MainWinViewModel _vmMainWin;
        public OverviewViewModel(MainWinViewModel vmMainWin)
        {
            _vmMainWin = vmMainWin;

            SortingPosibilities = new ObservableCollection<string>() 
            {
                "Týden",
                "Měsíc",
                "Rok",
                "Od počátku věků"
            };

            // stáhnout všechny budget data z databáze, kde sedí household id, vytvořit podle nejstaršího a nejmladšího datumu
            // a zvoleného sorting posibility vytvořit kolekci overviewColl, do které setřídit výdaje a příjmy, vypočíst celkový příjem,
            // celkový výdaj a bilanci

            GetOverviews();

            SelectedSortingPosibility = "Od počátku věků";
        }

        #region Properties

        private List<BudgetModel> AllBudgets { get; set; }

        public ObservableCollection<string> SortingPosibilities { get; set; }
        private string _selectedSortingPosibility;
        public string SelectedSortingPosibility 
        { 
            get => _selectedSortingPosibility; 
            set 
            { 
                if(value != null)
                {
                    _selectedSortingPosibility = value;
                    SortBudgets();
                    OnPropertyChanged(nameof(SelectedSortingPosibility));
                }
            } 
        }

        private string _selectedAuthor;
        public string SelectedAuthor
        {
            get => _selectedAuthor;
            set
            {
                if (value != null)
                {
                    _selectedAuthor = value;
                    SortBudgets();
                    OnPropertyChanged(nameof(SelectedAuthor));
                }
            }
        }

        private string _selectedCategory;
        public string SelectedCategory
        {
            get => _selectedCategory;
            set
            {
                if (value != null)
                {
                    _selectedCategory = value;
                    SortBudgets();
                    OnPropertyChanged(nameof(SelectedCategory));
                }
            }
        }

        public ObservableCollection<OverViewModel> OverviewCollection { get; set; }
        private OverViewModel _selectedOverview;
        public OverViewModel SelectedOverView 
        { 
            get => _selectedOverview; 
            set 
            {
                _selectedOverview = value;

                IncomesCollection = value?.Incomes;
                OutcomesCollection = value?.Outcomes;
                UpdateBalance();

                OnPropertyChanged(nameof(SelectedOverView));
            } 
        }


        private ObservableCollection<BudgetModel> _incomeCollection;
        public ObservableCollection<BudgetModel> IncomesCollection
        {
            get => _incomeCollection;
            set
            {
                _incomeCollection = value;
                OnPropertyChanged(nameof(IncomesCollection));
            }
        }

        private ObservableCollection<BudgetModel> _outcomesCollection;
        public ObservableCollection<BudgetModel> OutcomesCollection
        {
            get => _outcomesCollection;
            set
            {
                _outcomesCollection = value;
                OnPropertyChanged(nameof(OutcomesCollection));
            }
        }

        private ObservableCollection<string> _authors;
        public ObservableCollection<string> Authors
        {
            get => _authors;
            set
            {
                _authors = value;
                OnPropertyChanged(nameof(Authors));
            }
        }

        private ObservableCollection<string> _categories;
        public ObservableCollection<string> Categories
        {
            get => _categories;
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        private ObservableCollection<string> _itemNames;
        public ObservableCollection<string> ItemNames { get => _itemNames; set { _itemNames = value; OnPropertyChanged(nameof(ItemNames)); } }
        private string _selectedItemName;
        public string SelectedItemName { get => _selectedItemName; set { _selectedItemName = value; OnPropertyChanged(nameof(SelectedItemName)); SortBudgets(); } }

        private decimal _totalIncomes;
        public decimal TotalIncomes { get => _totalIncomes; set { _totalIncomes = value; OnPropertyChanged(nameof(TotalIncomes)); } }
        private decimal _totalOutcomes;
        public decimal TotalOutcomes { get => _totalOutcomes; set { _totalOutcomes = value; OnPropertyChanged(nameof(TotalOutcomes)); } }
        private decimal _finalBalance;
        public decimal FinalBalance { get => _finalBalance; set { _finalBalance = value; OnPropertyChanged(nameof(FinalBalance)); } }


        #endregion

        #region Commnands



        #endregion

        #region PrivateMethods

        private void GetOverviews() // stáhnout data z databáze
        {
            AllBudgets = new List<BudgetModel>();
            Authors = new ObservableCollection<string>() 
            {
                "Všichni"
            };
            SelectedAuthor = Authors.First();
            Categories = new ObservableCollection<string>()
            {
                "Vše"
            };
            SelectedCategory = Categories.First();
            ItemNames = new ObservableCollection<string>() { "Všechny" };
            SelectedItemName = ItemNames.First();

            int temp = Manager.Instance.LoggedInHouseHold.Id;

            var selection = $"Select * FROM Budget_Item WHERE Household_ID=\'{ temp }\'";
            SqlCommand selectionCommand = new SqlCommand(selection, Manager.Instance.SqlConnection);
            var dataReader = selectionCommand.ExecuteReader();

            while (dataReader.Read())
            {
                BudgetModel budget = new BudgetModel();
                budget.Id = Convert.ToInt32(dataReader.GetValue(0));
                budget.Name = dataReader.GetValue(1).ToString();
                if (!ItemNames.Contains(budget.Name))
                    ItemNames.Add(budget.Name);
                budget.Details = dataReader.GetValue(2).ToString();
                budget.Type = Convert.ToBoolean(dataReader.GetValue(3));
                budget.Value = Convert.ToDecimal(dataReader.GetValue(4));
                budget.Date = Convert.ToDateTime(dataReader.GetValue(5));
                budget.Author = dataReader.GetValue(6).ToString();
                if (!Authors.Contains(budget.Author))
                {
                    Authors.Add(budget.Author);
                }

                budget.Category = dataReader.GetValue(7).ToString();
                if (!Categories.Contains(budget.Category))
                {
                    Categories.Add(budget.Category);
                }

                AllBudgets.Add(budget);
            }
            dataReader.Close();
            CreateOverviews();
        }

        private void CreateOverviews() // vytvořit overviews nacpat do nich jednotlivé příjmy a výdaje
        {
            SelectedOverView = new OverViewModel();

            // příjmy:
            var incomes = AllBudgets.Where(x => x.Type == true);
            var outcomes = AllBudgets.Where(x => x.Type == false);

            foreach (var item in incomes)
            {
                SelectedOverView.Incomes.Add(item);
                TotalIncomes += item.Value;
            }

            foreach (var item in outcomes)
            {
                SelectedOverView.Outcomes.Add(item);
                TotalOutcomes += item.Value;
            }

            FinalBalance = TotalIncomes - TotalOutcomes;

            OnPropertyChanged(nameof(IncomesCollection));
            OnPropertyChanged(nameof(OutcomesCollection));
        }

        private void SortBudgets()
        {
            try
            {
                if(AllBudgets != null && AllBudgets.Count > 0)
                {
                    List<BudgetModel> NewAllBudgets = AllBudgets.OrderBy(x => x.Date).ToList();
                    OverviewCollection = new ObservableCollection<OverViewModel>();

                    DateTime first = Convert.ToDateTime(NewAllBudgets.First().Date);
                    first = first.AddDays(-(int)first.DayOfWeek);
                    first = first.AddDays(0);
                    first = new DateTime(first.Year, first.Month, first.Day, 0, 0, 0);

                    DateTime last = Convert.ToDateTime(NewAllBudgets.Last().Date);
                    last = new DateTime(last.Year, last.Month, last.Day, 23, 59, 59);

                    while (first <= last)
                    {
                        OverViewModel newModel = new OverViewModel(); // ( ͡° ͜ʖ ͡°)
                        List<BudgetModel> random = null;

                        switch (SelectedSortingPosibility)
                        {
                            case "Týden":
                                GregorianCalendar cal = new GregorianCalendar(GregorianCalendarTypes.Localized);
                                var week = cal.GetWeekOfYear(first, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                               

                                first = first.AddDays(-(int)first.DayOfWeek);
                                random = NewAllBudgets.Where(x => Convert.ToDateTime(x.Date) >= first && Convert.ToDateTime(x.Date) <= first.AddDays(6)).ToList();
                                newModel.Name = $"{week} týden {first.Year}";
                                first = first.AddDays(7);
                                break;

                            case "Měsíc":
                                first = new DateTime(first.Year, first.Month, 1);
                                random = NewAllBudgets.Where(x => Convert.ToDateTime(x.Date) >= first && Convert.ToDateTime(x.Date) <= first.AddMonths(1)).ToList();
                                newModel.Name = $"{(first.Month).ToString()}.{first.Year}";
                                first = first.AddMonths(1);
                                break;

                            case "Rok":
                                first = new DateTime(first.Year, 1, 1);
                                random = NewAllBudgets.Where(x => Convert.ToDateTime(x.Date) >= first && Convert.ToDateTime(x.Date) <= first.AddYears(1)).ToList();
                                newModel.Name = (first.Year).ToString();
                                first = first.AddYears(1);
                                break;

                            default:
                                random = NewAllBudgets;
                                first = last.AddMilliseconds(1);
                                newModel.Name = "Od počátku věků";
                                break;
                        }

                        if (SelectedAuthor != "Všichni" && random.Count > 0)
                        {
                            random = random.Where(x => x.Author == SelectedAuthor).ToList();
                        }

                        if (SelectedCategory != "Vše" && random.Count > 0)
                        {
                            random = random.Where(x => x.Category == SelectedCategory).ToList();
                        }
                        if (SelectedItemName != "Všechny" && random.Count > 0)
                            random = random.Where(x => x.Name == SelectedItemName).ToList();

                        newModel.Incomes = ListToObservableCollection(random.Where(x => x.Type == true).ToList());
                        newModel.Outcomes = ListToObservableCollection(random.Where(x => x.Type == false).ToList());

                        if (random.Count > 0)
                        {
                            OverviewCollection.Add(newModel);
                        }
                    }

                    if(OverviewCollection.Count > 0)
                        SelectedOverView = OverviewCollection.First();
                    else
                    {
                        OverviewCollection.Add(new OverViewModel("Žádné záznamy", new ObservableCollection<BudgetModel>(), new ObservableCollection<BudgetModel>(),
                            DateTime.MinValue, DateTime.MaxValue));
                        SelectedOverView = OverviewCollection.First();
                    }



                    OnPropertyChanged(nameof(OverviewCollection));
                    OnPropertyChanged(nameof(IncomesCollection));
                    OnPropertyChanged(nameof(OutcomesCollection));
                }
            }
            catch (Exception x)
            {
                log.Error(x, $"SortBudgets failed!!!");
                Messages.MsgError($"Neporařilo se setřídit záznamy - {x.Message}");
            }
        }

        private ObservableCollection<BudgetModel> ListToObservableCollection(List<BudgetModel> list)
        {
            ObservableCollection<BudgetModel> budgetModels = new ObservableCollection<BudgetModel>();

            foreach (var item in list)
            {
                budgetModels.Add(item);
            }

            return budgetModels;
        }

        public void UpdateBalance()
        {
            TotalIncomes = 0;
            TotalOutcomes = 0;

            foreach (var item in IncomesCollection)
            {
                TotalIncomes += item.Value;
            }

            foreach (var item in OutcomesCollection)
            {
                TotalOutcomes += item.Value;
            }

            FinalBalance = TotalIncomes - TotalOutcomes;
        }

        #endregion
    }
}
