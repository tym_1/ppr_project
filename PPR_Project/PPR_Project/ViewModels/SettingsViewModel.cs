﻿using PPR_Project.Models;
using PPR_Project.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace PPR_Project.ViewModels
{
    public class Clenove
    {
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public string Email { get; set; }
        public bool Admin { get; set; }
    }

    public class Limity
    {
        public string Kategorie { get; set; }
        public int Limit { get; set; }
    }

    public class Opakov
    {
        public string Nazev { get; set; }
        public int DobaOpak { get; set; }
    }

    public class SettingsViewModel : BaseViewModel
    {
        private static readonly NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
        private MainWinViewModel _vmMainWin;
        public SettingsViewModel(MainWinViewModel vmMainWin)
        {
            _vmMainWin = vmMainWin;

            ClenoveCollection = new ObservableCollection<Clenove>();
            ClenoveCollection.Add(new Clenove() { Jmeno = "Bob", Prijmeni = "Bobski", Email = "bobbobski@radnom.xyz", Admin = true});
            ClenoveCollection.Add(new Clenove() { Jmeno = "Pepa", Prijmeni = "Zdepa", Email = "pepazdema@sss.abc", Admin = false});

            LimityCollection = new ObservableCollection<Limity>();
            LimityCollection.Add(new Limity() { Kategorie="Nutné zásoby", Limit=5000 });
            LimityCollection.Add(new Limity() { Kategorie = "Potraviny", Limit = 10 });

            OpakovCollection = new ObservableCollection<Opakov>();
            OpakovCollection.Add(new Opakov() { Nazev = "Toaletní papír", DobaOpak = 2 });
            OpakovCollection.Add(new Opakov() { Nazev = "Elektrický proud - střídavý", DobaOpak = 31 });

            CommandDeleteHousehold = new CommandGeneric(DeleteHousehold);
            CommandDeleteHouseholdRecords = new CommandGeneric(DeleteRecordsConfirm);
        }

        #region Properties

        private ObservableCollection<Clenove> _clenoveCollection;
        public ObservableCollection<Clenove> ClenoveCollection
        {
            get => _clenoveCollection;
            set
            {
                _clenoveCollection = value;
                OnPropertyChanged(nameof(ClenoveCollection));
            }
        }

        private ObservableCollection<Limity> _limityCollection;
        public ObservableCollection<Limity> LimityCollection
        {
            get => _limityCollection;
            set
            {
                _limityCollection = value;
                OnPropertyChanged(nameof(LimityCollection));
            }
        }

        private ObservableCollection<Opakov> _opakovCollection;
        public ObservableCollection<Opakov> OpakovCollection
        {
            get => _opakovCollection;
            set
            {
                _opakovCollection = value;
                OnPropertyChanged(nameof(OpakovCollection));
            }
        }

        #endregion

        #region Commands

        public ICommand CommandDeleteHousehold { get; set; }
        public ICommand CommandDeleteHouseholdRecords { get; set; }


        public void DeleteRecordsConfirm()
        {
            MessageBoxResult result = MessageBox.Show("Opravdu chcete smazat všechny záznamy ze současné domácnosti?", "Smazat záznamy", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                DeleteRecords();
            }
        }

        public async void DeleteHousehold()
        {
            if (Messages.MsgYesNo("Opravdu chcete smazat současnou domácnost včetně všech záznamů?"))
            {
                DeleteRecords();

                try
                {
                    int temp = Manager.Instance.LoggedInHouseHold.Id;

                    var selection = $"DELETE FROM Household WHERE ID=\'{ temp }\'";
                    SqlCommand selectionCommand = new SqlCommand(selection, Manager.Instance.SqlConnection);
                    await selectionCommand.ExecuteNonQueryAsync();

                    Messages.MsgInfo("Domácnost a záznamy úspěšně odebrány.");
                    _vmMainWin.LogOut();
                    Manager.Instance.Settings.HouseholdName = null;
                    Manager.Instance.Settings.SerializeSettings();
                }
                catch (Exception ex)
                {
                    Messages.MsgError("Nepodařilo se smazat domácnost!");
                    log.Error(ex, "DeleteHousehold() failed!!!");
                }
            }
        }

        public async void DeleteRecords()
        {
            try
            {
                int temp = Manager.Instance.LoggedInHouseHold.Id;

                var selection = $"DELETE FROM Budget_Item WHERE Household_ID=\'{ temp }\'";
                SqlCommand selectionCommand = new SqlCommand(selection, Manager.Instance.SqlConnection);
                await selectionCommand.ExecuteNonQueryAsync();

                Messages.MsgInfo("Záznamy úspěšně odebrány.");
            }
            catch (Exception ex)
            {
                Messages.MsgError("Nepodařilo se smazat záznamy!");
                log.Error(ex, $"DeleteRecords() failed!!!");
            }
        }


        #endregion

        #region PrivateMethods

        #endregion


    }
}
