﻿using PPR_Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PPR_Project.Views
{
    /// <summary>
    /// Interaction logic for AddNewView.xaml
    /// </summary>
    public partial class AddNewBudgetView : UserControl
    {
        private AddNewBudgetViewModel vmAddNewBudget;
        public AddNewBudgetView(MainWinViewModel vmMainWin)
        {
            vmAddNewBudget = new AddNewBudgetViewModel(vmMainWin);
            this.DataContext = vmAddNewBudget;
            InitializeComponent();
        }
    }
}
