﻿using PPR_Project.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PPR_Project.Views
{
    /// <summary>
    /// Interakční logika pro OverviewView.xaml
    /// </summary>
    public partial class OverviewView : UserControl
    {
        private OverviewViewModel vmOverview;
        public OverviewView(MainWinViewModel vmMainWin)
        {
            vmOverview = new OverviewViewModel(vmMainWin);
            this.DataContext = vmOverview;
            InitializeComponent();
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
