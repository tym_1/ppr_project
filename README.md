# Navrhněte systém pro správu domácích výdajů

Tato aplikace bude sloužit k evidenci domácích **příjmů a výdajů.** Aplikace bude evidovat jednotlivé **kategorie nákladů**
(nájem, jednotlivé druhy energií, nákupy atd.),bude umožňovat **zápis jednotlivých položek** (případně opakovaných plateb v zadaném intervalu),
nákup bude možné přiřadit ke **konkrétním členům domácnosti.**  Aplikace bude poskytovat různé **přehledy,**
např. kolik bylo utraceno za položku „xy“ za nějaké časové období (obdobně pro příjmovou stránku domácnosti).
K zamyšlení: aplikace může být rozšířená o nastavení, kolik procent z příjmů může být utraceno za nájem,
potraviny atd. a toto nastavení bude aplikace hlídat. 



**ER diagramy & schéma:**

```
Link:	  https://erdplus.com/



Username: t_bartosik@utb.cz

Password: stonks1234


```

**Databáze:**
```

Username: A17133

Password: A17133


```